package chapter6.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import chapter6.beans.Message;
import chapter6.service.MessageService;

@WebServlet("/edit")
public class EditServlet extends HttpServlet {

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		HttpSession session = request.getSession();
		Message message = null;
		List<String> errorMessages = new ArrayList<String>();
		String id = request.getParameter("id");

		if(!StringUtils.isBlank(id) && id.matches("^[0-9]*$")) {
			message = new MessageService().select(Integer.parseInt(id));
		}
		if(message == null) {
			errorMessages.add("不正なパラメータが入力されました");
			session.setAttribute("errorMessages", errorMessages);
			response.sendRedirect("./");
            return;
		}
		request.setAttribute("message", message);
		request.getRequestDispatcher("edit.jsp").forward(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		String id = request.getParameter("id");
		String text = request.getParameter("text");
		Message message = new Message();
		message.setId(Integer.parseInt(id));
		message.setText(text);
		List<String> errorMessages = new ArrayList<String>();

		if(!isValid(errorMessages, text)) {
			request.setAttribute("errorMessages", errorMessages);
			request.setAttribute("message", message);
			request.getRequestDispatcher("edit.jsp").forward(request, response);
            return;
		}
	    new MessageService().update(message);
		request.setAttribute("message", message);
		response.sendRedirect("./");
	}

	private boolean isValid(List<String> errorMessages, String text) {

		if (StringUtils.isBlank(text)) {
            errorMessages.add("メッセージを入力してください");
        } else if (140 < text.length()) {
            errorMessages.add("140文字以下で入力してください");
        }
        if (errorMessages.size() != 0) {
            return false;
        }
        return true;
	}
}
