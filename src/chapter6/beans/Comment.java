package chapter6.beans;

import java.io.Serializable;
import java.util.Date;

public class Comment implements Serializable {

	private int id;
	private String text;
	private int userId;
	private int messageId;
	private Date createdDate;
    private Date updatedDate;

    //idのgetter,setter
    public int getId() {
    	return id;
    }
    public void setId(int id) {
    	this.id = id;
    }
    //textのgetter,setter
    public String getText() {
    	return text;
    }
    public void setText(String text) {
    	this.text = text;
    }
    //userIdのgetter,setter
    public int getUserId() {
    	return userId;
    }
    public void setUserId(int userId) {
    	this.userId = userId;
    }
    //messageIdのgetter,setter
    public int getMessageId() {
    	return messageId;
    }
    public void setMessageId(int messageId) {
    	this.messageId = messageId;
    }
    //createdDateのgetterとsetter
    public Date getCreatedDate() {
    	return createdDate;
    }
    public void setCreatedDate(Date createdDate) {
    	this.createdDate = createdDate;
    }
    //updatedDateのgetterとsetter
    public Date getUpdatedDate() {
    	return updatedDate;
    }
    public void setUpdatedDate(Date updatedDate) {
    	this.updatedDate = updatedDate;
    }
}
