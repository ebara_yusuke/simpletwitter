package chapter6.beans;

import java.util.Date;

public class UserComment {
	private int id;
	private String account;
	private String name;
	private int messageId;
	private int userId;
	private String text;
	private Date createdDate;

	//idのgetter,setter
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	//accountのgetter,setter
	public String getAccount() {
		return account;
	}
	public void setAccount(String account) {
		this.account = account;
	}
	//nameのgetter,setter
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	//userIdのgetterとsetter
	public int getUserId() {
		return userId;
	}
	public void setUserId(int userId) {
		this.userId = userId;
	}
	//textのgetterとsetter
	public String getText() {
		return text;
	}
	public void setText(String text) {
		this.text = text;
	}
	//createdDateのgetterとsetter
	public Date getCreatedDate() {
		return createdDate;
	}
	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}
	//messageIdのgetterとsetter
	public int getMessageId() {
		return messageId;
	}
	public void setMessageId(int messageId) {
		this.messageId = messageId;
	}
}
