package chapter6.beans;

import java.io.Serializable;
import java.util.Date;

public class Message implements Serializable {

    private int id;
    private int userId;
    private String text;
    private Date createdDate;
    private Date updatedDate;

    // getter/setterは省略されているので、自分で記述しましょう。

    //idのgetterとsetter
    public int getId() {
    	return id;
    }
    public void setId(int id) {
    	this.id = id;
    }

    //userIdのgetterとsetter
    public int getUserId() {
    	return userId;
    }
    public void setUserId(int userId) {
    	this.userId = userId;
    }

    //textのgetterとsetter
    public String getText() {
    	return text;
    }
    public void setText(String text) {
    	this.text = text;
    }

    //createdDateのgetterとsetter
    public Date getCreatedDate() {
    	return createdDate;
    }
    public void setCreatedDate(Date createdDate) {
    	this.createdDate = createdDate;
    }

    //updatedDateのgetterとsetter
    public Date getUpdatedDate() {
    	return updatedDate;
    }
    public void setUpdatedDate(Date updatedDate) {
    	this.updatedDate = updatedDate;
    }
}