package chapter6.service;

import static chapter6.utils.CloseableUtil.*;
import static chapter6.utils.DBUtil.*;

import java.sql.Connection;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang.StringUtils;

import chapter6.beans.Message;
import chapter6.beans.UserMessage;
import chapter6.dao.MessageDao;
import chapter6.dao.UserMessageDao;

public class MessageService {

    public void insert(Message message) {

        Connection connection = null;
        try {
            connection = getConnection();
            new MessageDao().insert(connection, message);
            commit(connection);
        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
    }

    public List<UserMessage> select(String userId, String startDate, String endDate) {
        final int LIMIT_NUM = 1000;
        Date date = new Date();
        SimpleDateFormat format = new SimpleDateFormat( "yyyy/MM/dd HH:mm:ss" );
        String nowDate = format.format(date);

        String start = null;
        String end = null;

        if(!StringUtils.isBlank(startDate)) {
        	start = startDate + " 00:00:00";
        } else {
        	start = "2020/01/01 00:00:00";
        }
        if(!StringUtils.isBlank(endDate)) {
        	end = endDate + " 23:59:59";
        } else {
        	end = nowDate;
        }

        Connection connection = null;
        try {
            connection = getConnection();
            Integer id = null;
            if((!StringUtils.isEmpty(userId)) && (userId.matches("^[0-9]+$"))) {
            	id = Integer.parseInt(userId);
            }
            List<UserMessage> messages = new UserMessageDao().select(connection, id, LIMIT_NUM, start, end);
            commit(connection);

            return messages;
        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
    }
    public void delete(String id) {

    	Connection connection = null;
    	try {
    		connection = getConnection();
    		new MessageDao().delete(connection, id);

    		commit(connection);

    	} catch(RuntimeException e) {
    		rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }

    }
    public Message select(int id) {
    	Connection connection = null;

    	try {
    		connection = getConnection();
    		Message message = new MessageDao().select(connection, id);
    		commit(connection);

    		return message;
    	} catch(RuntimeException e) {
    		rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }

    }
    public void update(Message message) {
    	Connection connection = null;
    	try {
    		connection = getConnection();
    		new MessageDao().update(connection,message);
    		commit(connection);
    	} catch (RuntimeException e) {
	        rollback(connection);
	        throw e;
	    } catch (Error e) {
	        rollback(connection);
	        throw e;
	    } finally {
	        close(connection);
	    }
    }
}